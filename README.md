
## Motivation 

On the Google Cloud Platform (GCP) it is currently not possible 
to automatically reduce disks in size. So if you have a temporary
peak in disk usage and have to increase the disk size (which 
can be done online), then you are stuck with a large disk.

This script tries to provide a automatic and safe way to shrink
GPC disks again. It does this using a temporary VM, the GCP's 
disk clone feature and the utility rsync. 

## Overview

The shrinker script is seperated into two scripted steps and 
two manual steps, first a hot sync preseeds the new disk during 
normal operatoin, and then a final 'cold sync' transfer the 
remaining diff while the service is stopped. The manual steps 
cover the actual switchover and the cleanup.

### Preparations

* Ensure all live disks have at least one snapshot (this speeds up the step 3)
* Create GCP workstation and install this utility (see chapter Setup) 

### Step 1: Hot sync, prepare new disk

* Setup a new disk, called the 'work disk' using the
  desired parameters (disk type and size)
* Create clone of the live disk, called 'clone disk'. The clone 
  represents a 'dirty' state, it is still in used by the application
* rsync all files from the clone disk to the work disk 

Step 1 can be run multiple times, it is important to do it once less
than 24h before the final sync.

### Step 2: Cold sync

* You stop the service and detach the disk (Stop the vm, or stop   
  the Kubernetes service). The disk must be in a clean state
* The sync script detects that the disk is 'cold' and does some 
  additional steps
* The sync script ensures that the fsck of the disk contains no errors
* The mtime of all files which were edited in the last 
  24 hours is set to now. Touching these files protect us against
  a race condition in the rsync process, where a file was 
  edited during the last hot sync.
* A 'final' snapshot of the live disk is created
* And finally: it prints out the instructions how to delete the old 
  live disk and 


### Step 3: Switchover

These steps are done manually by the user. The scripts prints 
out the exact steps

* Delete the old live disk
* Create the new live disk, as a clone of the 'work' disk
* Restart the service
   * k8s: Don't forget to delete and recreate your pv/pcs with the new size

### Step 4: Cleanup

These steps are also done manually.  

* Delete the work disk
* Delete all snapshots
 
## Setup VM/Workstation

Setup a vm, you can either use create-vm.sh or do it manually.

### VM Requirements

* Needs to be a google compute engine
* Same project and zone as the target disks
* Provision the machine with more CPUs to have higher IO throughput
* The script needs python 3.9, we recommend Ubuntu 22.04 or later

### Setup Script

**Setup dependencies**

```
sudo apt-get update
sudo apt-get install -y  python3-pip xxhash pv
```

**Checkout code and setup dependencies**

```
git clone https://gitlab.com/ergon-public/gcp-disk-shrinker.git
cd gcp-disk-shrinker
sudo pip install -r requirements.txt
```

**Login to GCP**

Either using a personal or a service account

```
sudo gcloud auth login
```


## Usage

```

# create an intial snapshot of the disk
## specify the same region as your disk for your storage-location 
gcloud compute disk create snapshot some-name \
    --storage-location europe-west1 \
    --source-disk rather-large-disk-now

# do hot sync, repeat multiple times
sudo python3 shrinker.py \
    --src-disk rather-large-disk-now \
    --target-size '250GB' sync

# stop service
kubectl scale sts some-important-service --replicas=0

# do a final cold sync
sudo python3 shrinker.py \
    --src-disk rather-large-disk-now \
    --target-size '250GB' sync

## follow instructions in Output! 

# start service again
kubectl scale sts some-important-service --replicas=1
