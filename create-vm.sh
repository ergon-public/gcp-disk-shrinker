#!/bin/bash

set -x
set -e


machine_type=t2d-standard-16

zone=$(gcloud config get compute/zone)
project=$(gcloud config get core/project)

# get first subnet, that ends with -vms
subnet=$(gcloud compute networks subnets list --format json | jq  -r '. [] | select (.name |endswith("-vms")) | .name'   | head -1)

# get first service account with display name 'Compute Engine default service account'
default_sa=$(gcloud iam service-accounts list --format json  | jq -r '. [] | select (.displayName == "Compute Engine default service account") | .email' | head -1)
machine_name="workstation-disk-shrinker"

# to search for latest gcloud compute images list | grep ubuntu-2204
image_name=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20220506

gcloud compute instances create "${machine_name}" \
        --project="${project}" \
        --zone="${zone}" \
        --machine-type=${machine_type} \
        --network-interface=network-tier=PREMIUM,subnet="${subnet}" \
        --maintenance-policy=MIGRATE \
        --provisioning-model=STANDARD \
        --service-account="${default_sa}" \
        --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
        --create-disk=auto-delete=yes,boot=yes,device-name=${machine_name},image=${image_name},mode=rw,size=10,type=projects/${project}/zones/${zone}/diskTypes/pd-balanced \
        --no-shielded-secure-boot \
        --shielded-vtpm \
        --shielded-integrity-monitoring \
        --reservation-affinity=any


gcloud compute config-ssh
