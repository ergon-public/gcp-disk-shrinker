from __future__ import annotations

import socket
import textwrap
import time

import click
from exec_utils import exec_strict, exec_strict_direct

from shrinker.disks_db import DiskDatabase
from shrinker.utils import creat_work_disk, create_clone_disk, delete_clone_or_work_disk, attach_disk, detach_disk, \
    format_work_disk, set_read_ahead, fsck, mount_disk, mount_path, create_snapshot
from shrinker.instances_db import InstanceDatabase
from shrinker.model import Disk
from shrinker.step_logger import step


def ensure_root():
    who_am_i = exec_strict(["whoami"]).strip()
    if who_am_i != "root":
        click.secho(f"ERROR: this script needs to be executed as user root, current user: '{who_am_i}'",
                    err=True, fg="red")
        exit(1)


def print_stats(disk: Disk, hash_verify: bool):
    mount_point_path = mount_path(disk)
    du_mb_raw = exec_strict(["du", "--apparent-size", "-cms", mount_point_path])
    du_mb = du_mb_raw.splitlines()[-1].split(" ")[0]
    files = exec_strict(["bash", "-c", f"find {mount_point_path} | wc -l"]).strip()
    if hash_verify:
        checksum_cmd = "tar -c -f - --sort=name . | pv | xxh128sum"
        exec_strict_direct(["bash", "-c", checksum_cmd], cwd=mount_point_path)

    click.echo(f"{disk.name:40s} {du_mb} MB {files} files")


class Shrinker:
    def __init__(self, /, workstation_name, instance_name, src_disk_name,
                 target_type, target_size):
        """
        :param workstation_name: name of the workstation, needed to attach/detach disks
        :param instance_name: unique name for the project, must be different if multiple disks are shrunk
                              in parallel on the same workstation
        :param src_disk_name: name of the source disk
        """
        ensure_root()
        self.target_size = target_size
        self.target_disk_type = target_type
        self.format_helper = "mkfs.ext4"
        self.src_disk_name = src_disk_name
        self.instance_name = instance_name
        self.workstation_name = workstation_name

        self.disks = DiskDatabase()
        self.instances = InstanceDatabase()

        self.fq_instance_name = f"{self.src_disk_name}-{instance_name}"

        self.clone_disk_name = f"{self.fq_instance_name}-clone"
        self.work_disk_name = f"{self.fq_instance_name}-work"

        self.workstation = self.instances[workstation_name]
        self.zone = self.workstation.zone_name

    def sync(self, keep_mounted, hash_verify):
        with step(f"Sync {self.fq_instance_name}", oneline=False):

            is_cold_sync, src_disk = self.check_and_lock_cold_sync()

            if not is_cold_sync:
                click.echo(
                    click.style("WARNING: Source disk is in use:", fg="cyan") + " "
                    + click.style("HOT-SYNC", bold=True, fg="magenta"))
            else:
                click.echo(
                    click.style("Source disk is cold:") + " "
                    + click.style("COLD-SYNC", bold=True, fg="green"))

            work_disk = self.prepare_and_mount_work_disk()
            clone_disk = self.create_and_mount_clone_disk(strict_fsck=is_cold_sync)

            with step("Sync data", oneline=False):
                if is_cold_sync:
                    # force rsync of all files which were written recently
                    # this fixes a rare problem with files which were written during the hot-sync and
                    # which seem to have the same size and mtime, but a different content
                    exec_strict(["bash", "-c", f"find {mount_path(clone_disk)} -mtime 0 -print0 | xargs -0 touch"])
                exec_strict_direct(["rsync",
                                    "--archive",
                                    "--sparse",
                                    "--info=progress2",
                                    "--hard-links",
                                    "--delete",
                                    "--delete-during",
                                    mount_path(clone_disk), mount_path(work_disk)])

            print_stats(clone_disk, hash_verify)
            print_stats(work_disk, hash_verify)

            if keep_mounted:
                click.echo(f"work: {mount_path(work_disk)}")
                click.echo(f"clone: {mount_path(clone_disk)}")
                click.secho("Warning: disks still mounted", fg="yellow")
                click.echo("")
                click.echo("    => run 'detach' to unmount and detach ")
                click.echo("")
            else:
                self.detach_and_delete_disk(clone_disk.name)
                detach_disk(self.workstation, work_disk)

            if is_cold_sync and not keep_mounted:
                detach_disk(self.workstation, src_disk)
                snapshot_name = f"{self.fq_instance_name}-{time.strftime('%Y-%m-%dt%H%M%S')}"
                create_snapshot(snapshot_name, src_disk, job_name=self.fq_instance_name)

                click.echo("")
                click.echo("Status: " + click.style("SUCCESS", fg="green"))
                click.echo(textwrap.dedent(f"""
                * Cold sync complete
                * Snapshot created ({snapshot_name})

                ## To complete the process

                # delete productive disk
                gcloud compute disks delete {src_disk.name} \\
                                --zone {src_disk.zone_name}

                # create productive disk using new disk
                gcloud compute disks create {src_disk.name} \\
                                --zone {src_disk.zone_name} \\
                                --type {self.target_disk_type} \\
                                --source-disk {self.work_disk_name}
                """))
                click.echo(textwrap.dedent(f"""
                ## To cleanup

                gcloud compute disks delete {self.work_disk_name} --zone {src_disk.zone_name}

                # list and all snapshots
                gcloud compute snapshots list --filter 'labels.shrinker-job={self.fq_instance_name}'

                # delete all snapshots
                gcloud compute snapshots delete snapshot-1 snapshot-2 # etc
                """))
            else:
                click.echo("")

    def check_and_lock_cold_sync(self) -> tuple[bool, Disk | None]:
        """
        try to ensure, that the source disk is 'cold'

        => check users
        => if there are none (or just ourself)
          => attach disk and return ture
        => if disk is in use
         => return false

        :return:
        """
        src_disk = self.disks.get(self.src_disk_name)
        if src_disk is None:
            raise RuntimeError(f"source disk {self.src_disk_name} not found")

        if self.is_cold(self.src_disk_name):
            attach_disk(self.workstation, src_disk)
            if not self.is_cold(self.src_disk_name):
                # this is strange, we should be the only vm attached to the vm
                raise RuntimeError("unable to ensure that disk is cold")

            return True, src_disk
        else:
            return False, None

    def is_cold(self, disk_name):
        self.disks.refresh()
        disk = self.disks[disk_name]

        current_users = set(disk.users if disk.users else [])
        if self.workstation.selfLink in current_users:
            current_users.remove(self.workstation.selfLink)

        return len(current_users) == 0

    def detach(self):
        with step("unmount and detach", oneline=False):
            self.detach_and_delete_disk(self.clone_disk_name)
            disk = self.disks.get(self.work_disk_name)
            if not disk:
                return
            detach_disk(self.workstation, disk)

    def create_and_mount_clone_disk(self, strict_fsck: bool) -> Disk:
        self.detach_and_delete_disk(self.clone_disk_name)

        create_clone_disk(clone_disk_name=self.clone_disk_name,
                          src_disk=self.disks[self.src_disk_name],
                          job_name=self.fq_instance_name)

        self.disks.refresh()

        clone_disk = self.disks[self.clone_disk_name]

        attach_disk(self.workstation, clone_disk)
        fsck(clone_disk, strict_fsck)
        set_read_ahead(clone_disk, 32768)
        mount_disk(clone_disk)
        return clone_disk

    def detach_and_delete_disk(self, disk_name):
        disk = self.disks.get(disk_name)
        if not disk:
            return

        detach_disk(self.workstation, disk)
        delete_clone_or_work_disk(disk)
        self.disks.refresh()

    def prepare_and_mount_work_disk(self) -> Disk:
        work_disk = self.disks.get(self.work_disk_name)

        if not work_disk:
            work_disk = self.setup_new_work_disk()
        else:
            attach_disk(self.workstation, work_disk)

        set_read_ahead(work_disk, 32768)
        mount_disk(work_disk)
        return work_disk

    def setup_new_work_disk(self) -> Disk:
        creat_work_disk(self.work_disk_name,
                        zone_name=self.zone,
                        size=self.target_size,
                        disk_type=self.target_disk_type,
                        job_name=self.fq_instance_name)
        self.disks.refresh()
        work_disk = self.disks[self.work_disk_name]
        attach_disk(self.workstation, work_disk)
        format_work_disk(work_disk, self.format_helper)
        return work_disk

    def detach_and_delete_work_disk(self):
        with step(f"Delete disk {self.work_disk_name}"):
            self.detach_and_delete_disk(self.work_disk_name)


@click.group()
@click.option('--workstation', help='name of workstation', default=socket.gethostname())
@click.option('--instance', help='unique name for this project', default="shrink")
@click.option('--src-disk', help='name of source disk', required=True)
@click.option('--target-size', help='desired size of target disk, units GB or TB, ie 15GB', required=True)
@click.option('--target-type', help='desired type of target disk, pd-stadard, pd-balanced, pd-ssd',
              default="pd-ssd",
              show_default=True)
@click.pass_context
def cli(ctx, src_disk, instance, workstation, target_type, target_size):
    ctx.ensure_object(dict)
    ctx.obj['SHRINKER'] = Shrinker(workstation_name=workstation,
                                   instance_name=instance,
                                   src_disk_name=src_disk,
                                   target_type=target_type,
                                   target_size=target_size)


@cli.command()
@click.pass_context
def delete_work_disk(ctx):
    shrinker: Shrinker = ctx.obj['SHRINKER']
    shrinker.detach_and_delete_work_disk()


@cli.command()
@click.option('--keep-mounted', help='keep the disk mounted, run detach to detach again',
              is_flag=True,
              show_default=True,
              default=False)
@click.option('--hash-verify', help='run a hash verification',
              is_flag=True,
              show_default=True,
              default=False)
@click.pass_context
def sync(ctx, keep_mounted, hash_verify):
    shrinker: Shrinker = ctx.obj['SHRINKER']
    shrinker.sync(keep_mounted=keep_mounted,
                  hash_verify=hash_verify)


@cli.command()
@click.pass_context
def detach(ctx):
    shrinker: Shrinker = ctx.obj['SHRINKER']
    shrinker.detach()


if __name__ == "__main__":
    cli()
