import json

from exec_utils import exec_strict

from shrinker.model import Disk


def disk_lookup():
    raw_disks = json.loads(exec_strict(["gcloud", "compute", "disks", "list", "--format", "json"]))
    disks = [Disk(**raw_disk) for raw_disk in raw_disks]

    return {d.name: d for d in disks}


class DiskDatabase:
    def __init__(self):
        self.refresh()

    def refresh(self):
        self.disks = disk_lookup()

    def get(self, name) -> Disk:
        return self.disks.get(name)

    def __getitem__(self, name) -> Disk:
        return self.disks[name]
