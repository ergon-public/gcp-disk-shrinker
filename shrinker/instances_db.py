import json

from exec_utils import exec_strict

from shrinker.model import Instance


def load_instance_lookup():
    raw_disks = json.loads(exec_strict(["gcloud", "compute", "instances", "list", "--format", "json"]))
    disks = [Instance(**raw_disk) for raw_disk in raw_disks]

    return {d.name: d for d in disks}


class InstanceDatabase:
    def __init__(self):
        self.refresh()

    def refresh(self):
        self.instances = load_instance_lookup()

    def get(self, name) -> Instance:
        return self.instances.get(name)

    def __getitem__(self, name) -> Instance:
        return self.instances[name]
