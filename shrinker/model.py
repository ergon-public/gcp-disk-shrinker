from typing import Optional

from pydantic import BaseModel


class Instance(BaseModel):
    name: str
    zone: str
    selfLink: str

    @property
    def zone_name(self):
        return self.zone.split("/")[-1]

    @property
    def region_name(self):
        return self.zone_name[0:-2]

    @property
    def fq_ident(self):
        return self.selfLink.replace("https://www.googleapis.com/compute/v1/", '')


class Disk(BaseModel):
    name: str
    type: str
    zone: str
    selfLink: str
    users: Optional[list[str]]

    @property
    def zone_name(self):
        return self.zone.split("/")[-1]

    @property
    def region_name(self):
        return self.zone_name[0:-2]

    @property
    def is_in_use(self):
        return self.users is not None and len(self.users) > 0
