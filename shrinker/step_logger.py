import time

import click


class Step:
    def __init__(self, msg, oneline=True):
        self.oneline = oneline
        self.msg = msg

    def __enter__(self):
        self.started = time.time()
        click.echo(f"{self.msg} ...", nl=not self.oneline)
        return None

    def __exit__(self, exc_type, exc_value, tb):
        duration = time.time() - self.started

        if exc_type is None:
            status_msg = click.style(f"OK ({duration:.3f}s)", fg="green")
        else:
            status_msg = click.style(f"ERROR ({duration:.3f}s)", fg="red")

        if self.oneline:
            click.echo(" " + status_msg)
        else:
            click.echo(click.style(self.msg) + " ... " + status_msg)


def step(msg, **kwargs):
    return Step(msg, **kwargs)
