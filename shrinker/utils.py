from __future__ import annotations

import os

from exec_utils import exec_strict
from exec_utils.exec_strict_error import ExecStrictError

from shrinker.model import Disk, Instance
from shrinker.step_logger import step


def creat_work_disk(target_disk_name, *, zone_name, size, disk_type, job_name: str):
    with step(f"Creating disk {target_disk_name} ({disk_type=}, {zone_name=}, {size=})"):
        exec_strict(["gcloud", "compute", "disks", "create", target_disk_name,
                     "--zone", zone_name,
                     "--type", disk_type,
                     "--labels", f"shrinker-job={job_name}",
                     "--size", size])


def create_clone_disk(clone_disk_name, src_disk: Disk, job_name: str):
    with step(f"Creating clone disk {clone_disk_name} from {src_disk.name}"):
        exec_strict(["gcloud", "compute", "disks", "create", clone_disk_name,
                     "--source-disk", src_disk.name,
                     "--labels", f"shrinker-job={job_name}",
                     "--zone", src_disk.zone_name,
                     "--type", src_disk.type])


def delete_clone_or_work_disk(disk: Disk):
    disk_name = disk.name
    if disk_name.endswith("-clone") or disk_name.endswith("-work"):
        with step(f"Deleting clone disk {disk_name}"):
            exec_strict(["gcloud", "compute", "disks", "delete", disk_name,
                         "--zone", disk.zone_name])
    else:
        raise RuntimeError(f"disk has invalid name and cannot be deleted: {disk_name}")


def attach_disk(instance, disk: Disk):
    cmd = ["gcloud", "compute", "instances", "attach-disk", instance.name,
           "--disk", disk.selfLink,
           "--zone", instance.zone_name,
           "--device-name", disk.name]

    with step(f"Attach disk {disk.name} to {instance.name}"):
        try:
            exec_strict(cmd)
        except ExecStrictError as ex:
            if f"is already being used by '{instance.fq_ident}'" in ex.stderr:
                pass
            else:
                raise


def detach_disk(instance: Instance, disk: Disk):
    umount_disk(disk)
    cmd = ["gcloud", "compute", "instances", "detach-disk", instance.name,
           "--disk", disk.selfLink,
           "--zone", instance.zone_name]

    with step(f"Detach disk {disk.name} from {instance.name}"):
        try:
            exec_strict(cmd)
        except ExecStrictError as ex:
            if 'is not attached to instance' in ex.stderr:
                pass
            else:
                raise


def disk_device(disk: Disk):
    return f"/dev/disk/by-id/google-{disk.name}"


def format_work_disk(disk, format_helper):
    if not disk.name.endswith("-work"):
        raise RuntimeError("not good")

    with step(f"Format {disk.name}"):
        exec_strict([format_helper, disk_device(disk)])


def set_read_ahead(disk: Disk, param):
    exec_strict(["blockdev", "--setra", str(param), disk_device(disk)])


def fsck(disk: Disk, strict_fsck: bool):
    with step(f"Run fsck {disk.name}"):
        if strict_fsck:
            # only allow one pass, and don't accept any errors
            raw_fsck(disk)
        else:
            try:
                raw_fsck(disk)
                # if first fsck is successfull we dont need to do another one
                return
            except ExecStrictError as ex:
                if "FILE SYSTEM WAS MODIFIED" in ex.stdout:
                    pass
                else:
                    raise ex

            raw_fsck(disk)


def raw_fsck(disk):
    exec_strict(["fsck.ext4", "-y", "-f", disk_device(disk)], log_console=False)


def mount_disk(disk):
    mount_point_path = mount_path(disk)

    if not os.path.exists(os.path.join(mount_point_path, "lost+found")):
        os.makedirs(mount_point_path, exist_ok=True)
        exec_strict(["mount", disk_device(disk), mount_point_path])


def umount_disk(disk: Disk):
    mount_point_path = mount_path(disk)
    if os.path.exists(os.path.join(mount_point_path, "lost+found")):
        exec_strict(["umount", mount_point_path])


def mount_path(disk):
    return "/mnt/" + disk.name + "/"


def create_snapshot(snapshot_name, disk, job_name: str):
    with step(f"create snapshot {snapshot_name} from {disk.name}"):
        cmd = ["gcloud", "compute", "snapshots", "create", snapshot_name,
               "--storage-location", disk.region_name,
               "--source-disk", disk.name,
               "--labels", f"shrinker-job={job_name}",
               "--source-disk-zone", disk.zone_name]
        exec_strict(cmd, log_console=True)
